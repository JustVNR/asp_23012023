﻿namespace _04_TagHelpers.Models
{
    public class Employe
    {
        public int Id { get; set; }

        public string Name { get; set; } = String.Empty;

        public double Salary { get; set; }

        public bool IsActif { get; set; } = true;

        public string Email { get; set; } = String.Empty;

        public EmployeType Type { get; set; } = EmployeType.DEBUTANT;

        public int DepartmentId { get; set; }
    }

    public enum EmployeType
    {
        DEBUTANT = 1,
        JUNIOR,
        SENIOR
    }
}
