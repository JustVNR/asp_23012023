﻿using _02_PassingData.Models;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

namespace _02_PassingData.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            if (TempData.ContainsKey("tmpDataFromAction"))
            {
                ViewBag.tmpDataFromAction = TempData["tmpDataFromAction"]?.ToString();
            }

            if (TempData.ContainsKey("tmpDataFromView"))
            {
                ViewBag.tmpDataFromView = TempData["tmpDataFromView"]?.ToString();
            }

            ViewBag.myCookie = Request.Cookies["key"];

            CookieOptions options = new()
            {
                Expires = DateTime.Now.AddSeconds(10)
            };

            Response.Cookies.Append("key", "cookie mima miam", options);

            Personnage p = new()
            {
                Nom = "duck",
                Id = 12,
                Prenom = "riri"
            };

            return View(p);
        }

        public IActionResult Privacy()
        {

            /*
             * ViewBag permet de transmettre des données du controlleur à la vue.
             * Ces données sont transmises en tant que propriétés de l'objet ViewBag
             * La portée du ViewBag est limitée à la requête actuelle : sa valeur est réinitialisée à null une fois transmise à la vue
             */
            ViewBag.Message = "Your application privacy page from ViewBag";

            /*
             * ViewData est un objet dictionnaire permettant de transmettre des données du controlleur à la vue.
             * Ces données sont tramsise sous forme de paires clés/valeurs.
             * Si on passe des objects complexes, il faut faire un transtypage et checker les valeurs nulles
             * La portée du ViewData est limitée à la requête actuelle : sa valeur est réinitialisée à null une fois transmise à la vue
             */
            ViewData["cle"] = "Your application privacy page from ViewData";

            // Attention : une propriété du ViewBag est reconnue en tant que clé du ViewDatat et réciproquement : ViewBag.Message <=> ViewData["Message"]

            IList<string> list = new List<string>
            {
                "liste",
                "de chaine de caractères",
                "passé au ViewDate"
            };

            ViewData["stringList"] = list;

            /*
             * TempData peut être utilisé pour transférer des données :
             * - d'un controlleur à une vue
             * - d'une vue à un controller
             * - d'une action à un autre action, dans le m^me controller ou pas
             * 
             * TempData stocke les données tant qu'elles ne sont paslues. 
             */
            TempData["tmpDataFromAction"] = "Data From Action 'Privacy' in 'HomeController' with TempData'";


            /* Session :
             * - Objet de type dictionnaire
             * - accessible par l'ensemble des controlleurs et des vues
             * - expire par défaut au bout de 20min
             * 
             * ATTENTIION : Il faut rajouterdans progrma.cs
             * - builder.Services.AddSession();
             * - app.UseSession();
             */
            HttpContext.Session.SetString("user_name", "titi");
            HttpContext.Session.SetInt32("user_id", 12);

            // Peut être prématurément vidé de tout ou partie de son contenu
            HttpContext.Session.Remove("user_id");
            HttpContext.Session.Clear();

            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}