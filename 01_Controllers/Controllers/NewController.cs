﻿using Microsoft.AspNetCore.Mvc;

namespace _01_Controllers.Controllers
{
    [Route("new_route")]
    public class NewController : Controller
    {

        private readonly IWebHostEnvironment _webHostEnvironment;

        public NewController(IWebHostEnvironment webHostEnvironment)
        {
            _webHostEnvironment = webHostEnvironment;
        }

        // /new/ActionReturnString/
        // /new/ActionReturnString/2
        // /new/ActionReturnString/2?firstName=riri&lastname=duck
        //[Route("new_action/{id?}")]
        // /new_action/2
        [Route("new_action/{id?}")]
        public string ActionReturnString(int? id)
        {

            string first = HttpContext.Request.Query["firstName"].ToString();
            string last = HttpContext.Request.Query["lastName"].ToString();

            return $"Hello from action 'ActionReturnString' of Controller 'New' with id = {id}, firstname = {first} and lastname={last}";
        }

        // /new/ActionReturnView
        public ViewResult ActionReturnView()
        {
            return View(); // retourne une vue qui porte le même nom que l'action
        }

        // /new/ActionReturnView
        public ViewResult ActionReturnSpecificView()
        {
            return View("SpecificView"); // retourne une vue nommée 'SpecificView' dans lle controller courant
        }

        // /new/ActionReturnRedirectToAction
        public ActionResult ActionReturnRedirectToAction()
        {
            return RedirectToAction("ActionReturnString");
        }

        // /new/ActionReturnRedirectToActionWithParameters
        public ActionResult ActionReturnRedirectToActionWithParameters()
        {
            return RedirectToAction("ActionReturnString", new {id=99, firstName = "loulou"});
        }

        // /new/ActionRedirectToRoute
        public ActionResult ActionRedirectToRoute()
        {
            return RedirectToRoute(new { controller ="Home", action = "Privacy"});
        }

        // /new/ActionReturnJson
        public ActionResult ActionReturnJson()
        {
            return Json("{key:value, key2:{key3:value3}}");
        }

        // /new/ActionReturnContent
        public ActionResult ActionReturnContent()
        {
            return Content("<div>Contenu de ma div</div>", "text/html");
        }

        // /new/ActionReturnJavascript
        public ActionResult ActionReturnJavascript()
        {
            return Content("<script>alert(\"Return Javascript !! \")</script>", "text/html");
        }

        // /new/ActionReturnNotFoundResult
        public ActionResult ActionReturnNotFoundResult()
        {
            return NotFound();
        }

        // /new/ActionReturnStatusCode
        public ActionResult ActionReturnStatusCode()
        {
            return StatusCode(StatusCodes.Status400BadRequest, "Mauvaise requête");
        }

        // /new/ActionReturnFilePathResult
        public FileResult ActionReturnFilePathResult()
        {
            string fileName = "site.css";

            string path = Path.Combine(_webHostEnvironment.WebRootPath, "css/", fileName);

            byte[] bytes = System.IO.File.ReadAllBytes(path);

            return File(bytes, "application/octet-stream", fileName);
        }

        // /new/ActionReturnFile // ATTENTION le nom de l'action ne contient pas le suffixe 'async'
        public async Task<FileResult> ActionReturnFileAsync()
        {
            string fileName = "site.css";

            string path = Path.Combine(_webHostEnvironment.WebRootPath, "css/", fileName);

            byte[] bytes = await System.IO.File.ReadAllBytesAsync(path);

            return File(bytes, "application/octet-stream", fileName);
        }
    }
}
