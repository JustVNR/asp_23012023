﻿using _01_Controllers.Models;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

/* Un controllur est une classe chargé de contrôler la mnière dont un utilisateur interragit avec une applicaion MVC
 * Il agit comme une gare de triage qui détermine le type de traitement à appliquer à la requête
 */
namespace _01_Controllers.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        private readonly IConfiguration _config;

        public HomeController(ILogger<HomeController> logger, IConfiguration config)
        {
            _logger = logger;
            _config = config;
        }


        // var mmonInstanceDeHomeController = new HomeController(logger, config);
        [ResponseCache(Duration = 5)]
        public IActionResult Index()
        {

            _logger.LogTrace("Trace Log");
            _logger.LogDebug("Debug Log");
            _logger.LogInformation("Information Log");
            _logger.LogWarning("Warning Log");
            _logger.LogError("Error Log");
            _logger.LogCritical("Critical Log");

            return View("Index", _config.GetValue<string>("testSettings"));
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}