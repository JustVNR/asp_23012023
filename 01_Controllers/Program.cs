// Cr�ation du WebApplicationBuilder
using System.Drawing;

var builder = WebApplication.CreateBuilder(args);

// ----------------------------------------------
// ----------- Add services to the container. ---
// ----------------------------------------------

builder.Services.AddControllersWithViews(); // On s'abonne ici au service 'AddControllersWithViews'

// On s'abonne au service de mise en cache
builder.Services.AddResponseCaching(options =>
{
    options.MaximumBodySize = 1024 * 1024;
});

// ----------------------------------------------
// ----------- Build Application. ---------------
// ----------------------------------------------
var app = builder.Build();

// ----------------------------------------------
// ---- Configure the HTTP request pipeline. ----
// ----------------------------------------------

// Le pipeline sp�cifie la mani�re dont l'application doit r�pondre � une requ�te
// Quand l'application re�oit une requ�te client, la requ�te fait un all�/retour dans le pipeline.
// Ce pipelin contient un ensemble de middlewares
// Ces middlewares permettent d'adresser diff�rents points techniques de la requ�te :
// - la gestion des erreurs 
// - la gestion des cookies
// l'authentification
// - le routage...

// Chaque middleware effectue une t�che unique
// L'ordre des middlewares est important car ils sont invoqu�s s�qquentiellement

if (!app.Environment.IsDevelopment())
{
    // redirection vers l'Action 'Error' du 'Controller'
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts(); // indique au client que le servur veut qu'il utiliser Https
}

app.UseHttpsRedirection();

app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

/*app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");*/

/*app.MapControllerRoute(
    name: "reverse",
    pattern: "{action=Privacy}/{controller=Home}/{id?}");*/

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=New}/{action=ActionReturnString}/{id?}");

app.UseResponseCaching();

app.Use(async (context, next) =>
{
    context.Response.GetTypedHeaders().CacheControl =
        new Microsoft.Net.Http.Headers.CacheControlHeaderValue()
        {
            Public = true,
            MaxAge = TimeSpan.FromSeconds(10)
        };

    await next();
});

// ----------------------------------------------
// ------------- Run Application. ---------------
// ----------------------------------------------
app.Run();
