﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace _07_Identity.Models.Configuration
{
    public class UserConfiguration : IEntityTypeConfiguration<ApplicationUser>
    {
        private const string ADMIN_ID = "B22698B8-42A2-4115-9631-1C2D1E2AC5F7";

        public void Configure(EntityTypeBuilder<ApplicationUser> builder)
        {
            var user = new ApplicationUser
            {
                Id = ADMIN_ID,
                Email = "admin@mvc.com",
                NormalizedEmail = "ADMIN@MVC.COM",
                EmailConfirmed = true,
                UserName =  "admin@mvc.com",
                NormalizedUserName = "ADMIN@MVC.COM"
            };

            user.PasswordHash = new PasswordHasher<ApplicationUser>().HashPassword(user, "Admin1234");

            builder.HasData(user);
        }
    }
}
