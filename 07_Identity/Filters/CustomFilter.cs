﻿using _07_Identity.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace _07_Identity.Filters
{
    public class CustomFilter : ActionFilterAttribute
    {
        private readonly UserManager<ApplicationUser> _userManager;

        public CustomFilter(UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
        }

        public override async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            var user = await _userManager.GetUserAsync(context.HttpContext.User);
            var path = context.HttpContext.Request.Path;
            var query = context.HttpContext.Request.QueryString;
            var pathAndQuery = path + query;

            if (user is null)
            {
                context.Result = new RedirectToRouteResult(
                    new RouteValueDictionary
                    {
                        {"controller", "Account" },
                        {"action", "Login" },
                        {"ReturnUrl", pathAndQuery }
                    });
            }
            else if (!await _userManager.IsInRoleAsync(user, "admin"))
            {
                context.Result = new RedirectToRouteResult(
                    new RouteValueDictionary
                    {
                        {"controller", "Account" },
                        {"action", "AccessDenied" }
                    });
            }
            else
            {
                await base.OnActionExecutionAsync(context, next);
            }
        }
    }
}
