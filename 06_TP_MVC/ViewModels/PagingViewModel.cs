﻿using _06_TP_MVC.Models;

namespace _06_TP_MVC.ViewModels
{
    public class PagingViewModel<T>
    {
        public string? Filter { get; set; }
        public int PageSize { get; set; } = 1;
        public int CurrentPage { get; set; } = 1;
        public int TotalRecords { get; set; }
        public List<T>? Items { get; set; }
        public string OrderBy { get; set; } = String.Empty;
        public SortDirection Direction { get; set; } = SortDirection.Asc;

        public SortDirection SwitchDirection()
        {
            return (Direction == SortDirection.Asc) ? SortDirection.Desc : SortDirection.Asc;
        }
    }

    public enum SortDirection
    {
        Asc,
        Desc
    }
}
