﻿using _06_TP_MVC.Models;
using _06_TP_MVC.Services;
using _06_TP_MVC.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace _06_TP_MVC.Controllers
{
    public class UsersController : Controller
    {
        private readonly IUsersServices _services;

        public UsersController(IUsersServices services)
        {
            _services = services;
        }

        // GET: Users
        public async Task<IActionResult> Index()
        {
            return View((await _services.GetAll()).Items);
        }

        // GET: Users/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var user = await _services.GetById((int)id); // (int)id <=> cast de int? vers int
            if (user == null)
            {
                return NotFound();
            }

            return View(user);
        }

        // GET: Users/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Users/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Email,Password,IsAdmin")] User user)
        {
            if (ModelState.IsValid)
            {
                await _services.Create(user);
                return RedirectToAction(nameof(Index));
            }
            return View(user);
        }

        // GET: Users/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var user = await _services.GetById((int)id);
            if (user == null)
            {
                return NotFound();
            }
            return View(user);
        }

        // POST: Users/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Email,Password,IsAdmin")] User user)
        {
            if (id != user.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    await _services.Update(user);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!UserExists(user.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(user);
        }

        // GET: Users/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var user = await _services.GetById((int)id);

            if (user == null)
            {
                return NotFound();
            }

            return View(user);
        }

        // POST: Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            await _services.Delete(id);

            return RedirectToAction(nameof(Index));
        }

        private bool UserExists(int id)
        {
            return _services.GetById(id) is not null;
        }

        // -----------------------------------------------
        // ------------------- PAGINATION ----------------
        // -----------------------------------------------

        public async Task<IActionResult> IndexPagined(
            [FromQuery] int p = 1,
            [FromQuery] int s = 3,
            [FromQuery] string q = ""
            )
        {
            PagingViewModel<User> vm = new()
            {
                Filter = q,
                PageSize = s,
                CurrentPage = p
            };

            return View(await _services.GetAll(vm));
        }
    }
}
