﻿using _06_TP_MVC.Services;
using _06_TP_MVC.ViewModels;
using LazZiya.TagHelpers.Alerts;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

namespace _06_TP_MVC.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IEmailServices _mail;

        public HomeController(ILogger<HomeController> logger, IEmailServices mail)
        {
            _logger = logger;
            _mail = mail;
        }

        public IActionResult Index()
        {
            _logger.LogCritical("TEST NLOG");

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        [HttpGet]
        public IActionResult Contact()
        {
            return View(new EmailViewModel());
        }

        [HttpPost]
        public async Task<IActionResult> Contact(EmailViewModel mail)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    await _mail.SendEmailAsync(mail);

                    TempData["success"] = "Message sent successfully !!";
                    TempData.Success("Message sent successfully !!");

                    return View(nameof(Index));
                }
                catch (Exception e)
                {
                    TempData["error"] = "Message could not be sent...";
                    TempData.Danger("Message could not be sent...");

                    _logger.LogError(e.Message);
                }
            }

            return View(mail);
        }
    }
}