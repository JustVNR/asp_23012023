﻿using _06_TP_MVC.ViewModels;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

namespace _06_TP_MVC.Controllers
{
    public class ErrorController : Controller
    {
        private readonly ILogger<ErrorController> _logger;

        public ErrorController(ILogger<ErrorController> logger)
        {
            _logger = logger;
        }

        [Route("error/{statusCode}")]
        public IActionResult HttpStatusCodeHandler(int statusCode)
        {
            var statusCodeResult = HttpContext.Features.Get<IStatusCodeReExecuteFeature>();

            switch(statusCode){

                case 404:

                    ViewBag.ErrorMessage = "Sorry, the ressource cound not be found";
                    ViewBag.Path = statusCodeResult?.OriginalPath;
                    ViewBag.QS = statusCodeResult?.OriginalQueryString;

                    _logger.LogWarning($"404 Error : Path = {statusCodeResult?.OriginalPath} - Query: {statusCodeResult?.OriginalQueryString}");

                    return View("NotFound");

                case 400:

                    ViewBag.ErrorMessage = "Bad Request";

                    return View("NotFound");

                default:

                    return View("Error", new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
            }
        }
    }
}
