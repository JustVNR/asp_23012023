﻿using _06_TP_MVC.Models;
using _06_TP_MVC.Services;
using _06_TP_MVC.Tools;
using _06_TP_MVC.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Org.BouncyCastle.Utilities;

namespace _06_TP_MVC.Controllers
{
    public class ProductsController : Controller
    {
        private readonly IProductsServices _services;

        private readonly IWebHostEnvironment _webHostEnvironment;


        public ProductsController(IProductsServices services, IWebHostEnvironment webHostEnvironment)
        {
            _services = services;
            _webHostEnvironment = webHostEnvironment;
        }

        // GET: Products
        public async Task<IActionResult> Index()
        {
            var products = (await _services.GetAll(new PagingViewModel<Product> { PageSize = 50})).Items;

            string uploads = Path.Combine(_webHostEnvironment.WebRootPath, "products", "mes_produits.csv");

            if (products is not null)
            {
                Serialize.ExportCSV(uploads, products);
            }

            return View(products);
        }

        // GET: Products/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id is null)
            {
                return new StatusCodeResult(StatusCodes.Status400BadRequest);
            }

            var product = await _services.GetById((int)id); // (int)id <=> cast de int? vers int

            if (product is null)
            {
                //return NotFound();
                Response.StatusCode = 404;
                return View("ProductNotFound", id.Value);
            }

            return View(product);
        }

        // GET: Products/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Products/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Description,Price")] Product product)
        {
            if (ModelState.IsValid)
            {
                await _services.Create(product);
                return RedirectToAction(nameof(Index));
            }
            return View(product);
        }

        // GET: Products/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var product = await _services.GetById((int)id);
            if (product == null)
            {
                return NotFound();
            }
            return View(product);
        }

        // POST: Products/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Description,Price")] Product product)
        {
            if (id != product.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    await _services.Update(product);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ProductExists(product.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(product);
        }

        // GET: Products/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var product = await _services.GetById((int)id);

            if (product == null)
            {
                return NotFound();
            }

            return View(product);
        }

        // POST: Products/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            await _services.Delete(id);

            return RedirectToAction(nameof(Index));
        }

        private bool ProductExists(int id)
        {
            return _services.GetById(id) is not null;
        }


        // -----------------------------------------------
        // ----------------- Ajax filterd ----------------
        // -----------------------------------------------

        public async Task<IActionResult> IndexFiltered()
        {
            return View((await _services.GetAll()).Items);
        }

        public async Task<IActionResult> _GetByDescription(string desc)
        {
            // return PartialView("_ProductsPage", await _dao.GetAll(desc));

            PagingViewModel<Product> vm = new()
            {
                Filter = desc
            };

            return PartialView("_ProductsPage", (await _services.GetAll(vm)).Items);
        }

        // -----------------------------------------------
        // --------- Ajax filterd Unobstrusive -----------
        // -----------------------------------------------

        public async Task<IActionResult> IndexFilteredUnobstrusive()
        {
            return View(await _services.GetAll());
        }


        public async Task<IActionResult> _IndexPartial(PagingViewModel<Product> vm)
        {
            return PartialView("_ProductsPage", (await _services.GetAll(vm)).Items);
        }

        // -----------------------------------------------
        // ------------------- PAGINATION ----------------
        // -----------------------------------------------

        public async Task<IActionResult> IndexPagined(
            [FromQuery] int p = 1,
            [FromQuery] int s = 3,
            [FromQuery] string q = ""
            )
        {
            PagingViewModel<Product> vm = new()
            {
                Filter = q,
                PageSize = s,
                CurrentPage = p
            };

            return View(await _services.GetAll(vm));
        }


        public async Task<IActionResult> IndexAjaxPagined(
           [FromQuery] int p = 1,
           [FromQuery] int s = 3,
           [FromQuery] string q = ""
           )
        {
            var model = await _services.GetAll(new PagingViewModel<Product>()
            {
                Filter = q,
                PageSize = s,
                CurrentPage = p
            });

            bool isAjax = Request.Headers["X-Requested-With"] == "XMLHttpRequest";

            if (isAjax is true)
            {
                return PartialView("_IndexPartialPagined", model);
            }

            return View(model);
        }

        // -----------------------------------------------
        // --------------------- SCROLL ------------------
        // -----------------------------------------------

        public async Task<IActionResult> IndexScroll()
        {
            return View(await _services.GetAll(new PagingViewModel<Product>() { PageSize = 14}));
        }

        public async Task<IActionResult> _IndexScrollPartial(PagingViewModel<Product> vm)
        {
            return PartialView("_IndexScrollPartial", await _services.GetAll(vm));
        }

        public async Task<IActionResult> _NextPage(PagingViewModel<Product> vm)
        {
            vm.CurrentPage++;

            return PartialView("_ProductsPage", (await _services.GetAll(vm)).Items);
        }


        // -----------------------------------------------
        // ------------ PAGINED AND SORTED ---------------
        // -----------------------------------------------

        public async Task<IActionResult> IndexPaginedAndSorted(
           [FromQuery] int p = 1,
           [FromQuery] int s = 2,
           [FromQuery] string q = "",
           [FromQuery] string sort = "description",
           [FromQuery] SortDirection direction = SortDirection.Asc)
        {
            PagingViewModel<Product> vm = new()
            {
                Filter = q,
                PageSize = s,
                CurrentPage = p,
                OrderBy = sort,
                Direction = direction
            };

            return View(await _services.GetAll(vm));
        }
    }
}
