using _06_TP_MVC.DAO;
using _06_TP_MVC.Models;
using _06_TP_MVC.Services;
using _06_TP_MVC.Settings;
using Microsoft.EntityFrameworkCore;
using NLog.Extensions.Logging;

var builder = WebApplication.CreateBuilder(args);


builder.Logging.AddNLog();

// Add services to the container.
builder.Services.AddControllersWithViews();

//string? connectionString = builder.Configuration.GetConnectionString("DefaultConnection");
string? connectionString = builder.Configuration.GetValue<string>("DefaultConnection");

builder.Services.AddDbContext<ApplicationDbContext>(options => options.UseSqlServer(connectionString));

builder.Services.Add(new ServiceDescriptor(typeof(EmailSettings),
                                        c => builder.Configuration.GetSection("MailSettings").Get<EmailSettings>(),
                                        ServiceLifetime.Singleton));

builder.Services.AddScoped<IProductsDAO, ProductsDAO>();
builder.Services.AddScoped<IProductsServices, ProductsServices>();

builder.Services.AddScoped<IUsersDAO, UsersDAO>();
builder.Services.AddScoped<IUsersServices, UsersServices>();

builder.Services.AddScoped<IEmailServices, EmailServices>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();

    app.UseStatusCodePagesWithReExecute("/Error/{0}");
}

app.UseHttpsRedirection();

// app.UseRequestLocalization("fr-FR");
app.UseRequestLocalization("en-US");
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

app.Run();
