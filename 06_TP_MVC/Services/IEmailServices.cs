﻿using _06_TP_MVC.ViewModels;

namespace _06_TP_MVC.Services
{
    public interface IEmailServices
    {
        Task SendEmailAsync(EmailViewModel mailRequest);
    }
}
