﻿using _06_TP_MVC.Settings;
using _06_TP_MVC.ViewModels;
using MailKit.Net.Smtp;
using MimeKit;

namespace _06_TP_MVC.Services
{
    public class EmailServices : IEmailServices
    {
        private readonly EmailSettings _mailSettings;

        public EmailServices(EmailSettings mailSettings)
        {
            _mailSettings = mailSettings;
        }

        public async Task SendEmailAsync(EmailViewModel mailRequest)
        {
            MimeMessage email = new();

            email.From.Add(new MailboxAddress(mailRequest.FromEmail, _mailSettings.Mail));
            email.To.Add(MailboxAddress.Parse(_mailSettings.Mail));

            email.Subject = mailRequest.Subject;

            var builder = new BodyBuilder
            {
                // TODO Attachements...

                HtmlBody = mailRequest.Body
            };

            if (mailRequest.Attachements is not null)
            {
                byte[] fileBytes;

                foreach (var file in mailRequest.Attachements)
                {
                    if (file.Length > 0)
                    {
                        using (MemoryStream ms = new())
                        {
                            file.CopyTo(ms);// copie du fichier dans le flux
                            fileBytes = ms.ToArray(); // conversion du flux en tableau
                        }
                        builder.Attachments.Add(file.FileName, fileBytes, ContentType.Parse(file.ContentType));
                    }
                }
            }

            email.Body = builder.ToMessageBody();

            using (SmtpClient smtp = new())
            {
                smtp.Connect(_mailSettings.Host, _mailSettings.Port, MailKit.Security.SecureSocketOptions.StartTls);

                // https://www.ionos.fr/digitalguide/email/aspects-techniques/starttls/

                smtp.Authenticate(_mailSettings.Mail, _mailSettings.Password);

                await smtp.SendAsync(email);
                // smtp.Dispose(); // Déja fait par le using...
            }
        }
    }
}
