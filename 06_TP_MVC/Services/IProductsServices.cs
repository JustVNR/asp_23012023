﻿using _06_TP_MVC.Models;
using _06_TP_MVC.ViewModels;

namespace _06_TP_MVC.Services
{
    public interface IProductsServices
    {
        Task<PagingViewModel<Product>> GetAll(PagingViewModel<Product>? model = null);
        Task<Product?> GetById(int id);
        Task Create(Product p);
        Task Update(Product p);
        Task Delete(int id);
    }
}
