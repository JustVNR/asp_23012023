﻿using _06_TP_MVC.Models;
using _06_TP_MVC.ViewModels;

namespace _06_TP_MVC.Services
{
    public interface IUsersServices
    {
        Task<PagingViewModel<User>> GetAll(PagingViewModel<User>? model = null);
        Task<User?> GetById(int id);
        Task Create(User p);
        Task Update(User p);
        Task Delete(int id);
    }
}
