﻿using _06_TP_MVC.DAO;
using _06_TP_MVC.Models;
using _06_TP_MVC.Tools;
using _06_TP_MVC.ViewModels;

namespace _06_TP_MVC.Services
{
    public class ProductsServices : IProductsServices
    {
        private readonly IProductsDAO _dao;

        public ProductsServices(IProductsDAO dao)
        {
            _dao = dao;
        }

        public async Task Create(Product p)
        {
            await _dao.Create(p);
        }

        public async Task Delete(int id)
        {
            await _dao.Delete(id);
        }

        public async Task<PagingViewModel<Product>> GetAll(PagingViewModel<Product>? model = null)
        {
            return await _dao.GetAll(model);
        }

        public async Task<Product?> GetById(int id)
        {
            return await _dao.GetById(id);
        }

        public async Task Update(Product p)
        {
            await _dao.Update(p);
        }
    }
}
