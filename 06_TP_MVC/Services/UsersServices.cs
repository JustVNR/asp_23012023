﻿using _06_TP_MVC.DAO;
using _06_TP_MVC.Models;
using _06_TP_MVC.Tools;
using _06_TP_MVC.ViewModels;

namespace _06_TP_MVC.Services
{
    public class UsersServices : IUsersServices
    {

        private readonly IUsersDAO _dao;

        public UsersServices(IUsersDAO dao)
        {
            _dao = dao;
        }

        public async Task Create(User u)
        {
            u.Password = Hasher.HashPassword(u.Password);

            await _dao.Create(u);
        }

        public async Task Delete(int id)
        {
            await _dao.Delete(id);
        }

        public async Task<PagingViewModel<User>> GetAll(PagingViewModel<User>? model = null)
        {
            return await _dao.GetAll(model);
        }

        public async Task<User?> GetById(int id)
        {
            return await _dao.GetById(id);
        }

        public async Task Update(User u)
        {
            await _dao.Update(u);
        }
    }
}
