﻿using _06_TP_MVC.Models;
using _06_TP_MVC.ViewModels;
using Microsoft.EntityFrameworkCore;
using NuGet.Protocol;

namespace _06_TP_MVC.DAO
{
    public class ProductsDAO : IProductsDAO
    {
        private readonly ApplicationDbContext _db;

        public ProductsDAO(ApplicationDbContext db)
        {
            _db = db;
        }

        public async Task Create(Product p)
        {
            _db.Add(p);
            await _db.SaveChangesAsync();
        }

        public async Task Delete(int id)
        {
            var product = await _db.Products.FindAsync(id);

            if (product != null)
            {
                _db.Products.Remove(product);
                await _db.SaveChangesAsync();
            }
        }

        /* public async Task<List<Product>> GetAll(string? description)
         {
             if (String.IsNullOrEmpty(description))
             {
                 return await _db.Products.ToListAsync();
             }
             return await _db.Products.Where(x => x.Description.Contains(description)).ToListAsync();
         }*/

        /*public async Task<List<Product>> GetAll(ProductsPagingViewModel? model)
        {
            *//*if (model is null)
               {
                   model = new ProductsPagingViewModel
                   {
                       Filter = String.Empty
                   };
               }*/

            /*model ??= new ProductsPagingViewModel
                {
                    Filter = String.Empty
                };*//*

            model ??= new();

            model.Filter ??= String.Empty;

            if (String.IsNullOrEmpty(model.Filter))
            {
                return await _db.Products.ToListAsync();
            }
            return await _db.Products.Where(x => x.Description.Contains(model.Filter)).ToListAsync();
        }*/

        public async Task<PagingViewModel<Product>> GetAll(PagingViewModel<Product>? model = null)
        {
            model ??= new();

            model.Filter ??= String.Empty;

            model.TotalRecords = await _db.Products.AsNoTracking().Where(x => x.Description.Contains(model.Filter)).CountAsync();

            model.Items = await _db.Products.AsNoTracking()
                                                .Where(x => x.Description.Contains(model.Filter))
                                                //.OrderBy(x => x.Price)
                                                .OrderBy(model.OrderBy, model.Direction) // Méthode d'extension définie dans la classe Extensions
                                                .Skip((model.CurrentPage - 1) * model.PageSize)
                                                .Take(model.PageSize)
                                                .ToListAsync();
            return model;
        }

        public async Task<Product?> GetById(int id)
        {
            // return await _db.Products.FirstOrDefaultAsync(m => m.Id == id);
            return await _db.Products.FindAsync(id);
        }

        public async Task Update(Product p)
        {
            _db.Update(p);
            await _db.SaveChangesAsync();
        }
    }
}
