﻿using _06_TP_MVC.Models;
using _06_TP_MVC.ViewModels;

namespace _06_TP_MVC.DAO
{
    public interface IProductsDAO
    {
        // Task<List<Product>> GetAll(ProductsPagingViewModel? model = null);
        Task<PagingViewModel<Product>> GetAll(PagingViewModel<Product>? model = null);
        Task<Product?> GetById(int id);
        Task Create(Product p);
        Task Update(Product p);
        Task Delete(int id);
    }
}
