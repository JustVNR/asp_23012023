﻿using _06_TP_MVC.Models;
using _06_TP_MVC.ViewModels;

namespace _06_TP_MVC.DAO
{
    public interface IUsersDAO
    {
        //Task<List<User>> GetAll();
        Task<PagingViewModel<User>> GetAll(PagingViewModel<User>? model = null);
        Task<User?> GetById(int id);
        Task Create(User u);
        Task Update(User u);
        Task Delete(int id);
    }
}
