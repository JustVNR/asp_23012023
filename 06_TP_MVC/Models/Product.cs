﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace _06_TP_MVC.Models
{
    public class Product
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string Description { get; set; } = String.Empty;

        [Required]
        [DataType(DataType.Currency)]
        [Column(TypeName ="decimal(18,2)")]
        public decimal Price { get; set; }

        // Entity Framework a besoin d'un constructeur sans paramètre !!
        public Product()
        {

        }

        public Product(int id, string description, decimal price)
        {
            Id = id;
            Description = description;
            Price = price;
        }
    }
}
