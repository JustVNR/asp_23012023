﻿using Microsoft.EntityFrameworkCore;

namespace _06_TP_MVC.Models
{
    public class ApplicationDbContext : DbContext // Installer Entity Framework
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options) // Appel au constructeur de la classe mère
        {

        }

        public DbSet<Product> Products { get; set; }
        public DbSet<User> Users { get; set; }
    }
}
