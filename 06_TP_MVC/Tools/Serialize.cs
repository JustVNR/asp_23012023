﻿using _06_TP_MVC.Models;

namespace _06_TP_MVC.Tools
{
    public class Serialize
    {
        //Sauvegarde CSV
        public static void ExportCSV(string path, List<Product> listPrd)
        {
            using (StreamWriter sw = new(path))
            {
                foreach (Product p in listPrd)
                {
                    sw.WriteLine(p.Id + ";" + p.Description + ";" + p.Price);
                }
            }
        }

        //Restauration CSV
        public static List<Product> ImportCSV(string path)
        {
            using (StreamReader sr = new(path))
            {
                List<Product> listPrd = new();

                //Lire une ligne et la découper
                while (!sr.EndOfStream)
                {
                    string? line = sr.ReadLine();

                    if (line is null) continue;

                    string[] values = line.Trim().Split(';');

                    Product cb = new()
                    {
                        Id = Convert.ToInt32(values[0]),

                        Description = values[1],

                        Price = Convert.ToDecimal(values[2])
                    };

                    listPrd.Add(cb);
                }

                return listPrd;
            }
        }
    }
}
