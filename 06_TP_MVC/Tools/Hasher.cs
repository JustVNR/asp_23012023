﻿using System.Security.Cryptography;
using System.Text;

namespace _06_TP_MVC.Tools
{
    public class Hasher
    {
        public static string HashPassword(string pwd)
        {
            using (SHA256 sha256 = SHA256.Create())
            {
                byte[] bytes = sha256.ComputeHash(Encoding.UTF8.GetBytes(pwd));

                StringBuilder builder = new();

                for (int i = 0; i < bytes.Length; i++)
                {
                    // https://www.programmerall.com/article/9035343673/

                    builder.Append(bytes[i].ToString("x2")); // x2 => x: hexadecimal / 2: 2 digits
                }

                return builder.ToString();
            }
        }
    }
}
