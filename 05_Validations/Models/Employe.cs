﻿using System.ComponentModel.DataAnnotations;

namespace _05_Validations.Models
{
    public class Employe
    {
        [Required] // Champ obligatoire
        [Display(Name = "User Name")]
        public string UserName { get; set; } = String.Empty;

        [Required(ErrorMessage = "Mot de passe obligatoire")]
        [DataType(DataType.Password)]
        public string Password { get; set; } = String.Empty;

        [Required]
        [DataType(DataType.Date)]
        public DateTime DateNaissance { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; } = String.Empty;

        [Required]
        [Range(1, 10)]
        public int Evaluation { get; set; }

        [RegularExpression(@"^(\d{10})$", ErrorMessage = "Le numéro de tel doit être composé de 10 chiffres")]
        public string Telephone { get; set; } = "0000000000";

        [Required]
        [DataType(DataType.MultilineText)]
        public string Commentaire { get; set; } = String.Empty;

        [Display(Name = "Upload Avatar")]
        [CustomAvatarValidation]
        public IFormFile? File { get; set; }
    }

    public class CustomAvatarValidationAttribute : ValidationAttribute
    {
        public override bool IsValid(object? value)
        {
            int MaxContentLength = 1024 * 1024;

            string[] AllowedExtensions = new string[] { ".jpg", ".jpeg", ".png" };

            if (value is not IFormFile file)
            {
                return false;
            }

            if (!AllowedExtensions.Contains(file.FileName[file.FileName.LastIndexOf('.')..]))
            {
                ErrorMessage = $"Allowed extensions {String.Join(", ", AllowedExtensions)}";
                return false;
            }

            if (file.Length > MaxContentLength)
            {
                ErrorMessage = $"Your photo is too large, maximum allowed size is {MaxContentLength / 1024 / 1024} MB";
                return false;
            }

            return true;
        }
    }
}
