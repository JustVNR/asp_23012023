﻿using _05_Validations.Models;
using Microsoft.AspNetCore.Mvc;

namespace _05_Validations.Controllers
{
    public class EmployeController : Controller
    {
        private readonly IWebHostEnvironment _webHostEnvironment;

        public EmployeController(IWebHostEnvironment webHostEnvironment)
        {
            _webHostEnvironment = webHostEnvironment;
        }

        public IActionResult FormValidation()
        {
            return View(new Employe());
        }

        [ValidateAntiForgeryToken] // https://learn.microsoft.com/fr-fr/aspnet/core/security/anti-request-forgery?view=aspnetcore-7.0
        [HttpPost]
        public async Task<IActionResult> FormValidation(Employe emp)
        {
            if(ModelState.IsValid)
            {
                string uploads = Path.Combine(_webHostEnvironment.WebRootPath, "uploads");

                if (emp.File is not null && emp.File.Length > 0)
                {
                    string filePath = Path.Combine(uploads, emp.File.FileName);

                    using (Stream fs = new FileStream(filePath, FileMode.Create))
                    {
                        await emp.File.CopyToAsync(fs);

                        // fs.Close(); Implicite car fs créé dans une clase 'using'
                    }
                }

                return View("Details", emp);
            }

            return View(emp);
        }
    }
}
